import turtle

def flags():
	try:
		turtle.TurtleScreen._RUNNING = True
		turtle.screensize(canvwidth=800, canvheight=800, bg=None)
		x = -30; y = 0
		w = turtle.Screen()
		w.clear()
		w.bgcolor("#ffffff")
		t = turtle.Turtle()

		turtle.tracer(0, 0)
		# - - - - -
		t.goto(0,0)
	
		t.seth(0)
		t.forward(100)
		
		t.goto(0,0)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		t.goto(0,50)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		t.goto(0,100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		t.goto(25,100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
			
		t.goto(50,100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		t.goto(50,50)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
				
		t.goto(50,0)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
				
		t.goto(100,100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		t.goto(100,50)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		t.goto(100,0)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		
		
		t.goto(150,0)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		t.goto(150,50)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		t.goto(150,100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		

			
		t.goto(-50,0)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		t.goto(200,0)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		

		
		
		t.goto(100,0)
		t.seth(90)
		t.forward(100)
		t.pencolor('#00FF00')
		t.fillcolor('#00FF00')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		
				
		t.goto(0,-50)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
					
		t.goto(50,-50)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		t.goto(100,-50)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()		
		
		t.goto(150,-50)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		
		
				
		t.goto(0,-100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
					
		t.goto(50,-100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		t.goto(100,-100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()		
		
		t.goto(150,-100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
				
		t.goto(0,-150)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
					
		t.goto(50,-150)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		t.goto(100,-150)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()		
		
		t.goto(150,-150)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		
	
		
				
		t.goto(-25,-100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
						
		t.goto(175,-100)
		t.seth(90)
		t.forward(100)
		t.pencolor('#FFC064')
		t.fillcolor('#FFC064')
		t.begin_fill()
		for n in range (0,4):
			t.forward(50)
			t.left(90)
		t.end_fill()
		
		
		
		turtle.update()
		w.exitonclick()
	finally:
		turtle.Terminator()

def main():
	flags()


if __name__ == '__main__':
	main()

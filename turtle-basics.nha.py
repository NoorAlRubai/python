import turtle
# main function
def main():
	screen = turtle.Screen()
	t = turtle.Turtle()
	#t.tracer(0, 0)
	#turtle pencolor("#290000")red
	t.goto(-200,100)
	t.fd(0)
	t.lt(50)
	t.pencolor("#C80098")#green
	t.width(2)
	t.bk(0)
	t.rt(45)
	t.rt(90)
	t.goto(200,100)
	t.pencolor("#0000ff")#blue
	t.width(2)
	t.goto(0,0)
	t.penup()

	# fill poly start
	t.goto(200,0)
	t.pendown()
	t.pencolor("#00ffff")#cyan
	t.begin_fill()
	t.fd(100)
	t.bk(0)
	t.rt(0)
	t.fd(25)
	t.rt(0)
	t.goto(200,100)
	t.fd(0)
	t.rt(0)
	t.fd(0)
	t.end_fill()
	


	t.penup()
	t.goto(-200,100)
	t.pendown()
	t.pencolor("#ff00ff")#magenta
	t.fillcolor("#ff7f00")#orange
	t.begin_fill()
	t.fd(100)
	t.rt(0)
	t.fd(100)
	t.rt(0)
	t.fd(0)
	t.rt(0)
	t.fd(0)
	t.end_fill()
	t.pencolor("#210002")
	t.pendown()
	t.goto(200,-100)
	t.fillcolor("#000000")
	t.begin_fill()
	t.end_fill()
	
	screen.exitonclick()	
	

if __name__ == "__main__":
		main()

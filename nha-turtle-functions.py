#save file as turtle-basics.py
import turtle
# main function

def myblock(t,pcolor,fcolor):
	t.pencolor(pcolor)
	t.fillcolor(fcolor)
	t.begin_fill()
	t.fd(200)
	t.rt(150)
	t.fd(200)
	t.rt(150)
	t.fd(200)
	t.rt(150)
	t.fd(200)
	t.end_fill()
	

def main():
	screen = turtle.Screen()
	t = turtle.Turtle()
	#turtle.tracer(0, 0)
	t.width(5)
	#myblock(t,fillcolor,pencolor)
	
	
	t.goto(-100,200)
	pcolor = "#EE405E";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	
	t.goto(100,100)
	pcolor = "#070707";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(-100,100)
	pcolor = "#000000";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(-100,-100)
	pcolor = "#000000";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(100,-100)
	pcolor = "#007fff";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	t.goto(100,-100)
	pcolor = "#EE405E";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	
	
	pcolor = "#EE405E";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	t.goto(100,100)
	pcolor = "#070707";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(-100,100)
	pcolor = "#000000";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(-100,-100)
	pcolor = "#000000";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(100,-100)
	pcolor = "#007fff";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	t.goto(100,-100)
	pcolor = "#EE405E";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	
	pcolor = "#EE405E";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	t.goto(100,100)
	pcolor = "#070707";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(-100,100)
	pcolor = "#000000";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(-100,-100)
	pcolor = "#000000";fcolor = "#000000"
	myblock(t,pcolor,fcolor)
	t.goto(100,-100)
	pcolor = "#007fff";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	t.goto(100,-100)
	pcolor = "#EE405E";fcolor = "#000000"
	myblock(t,fcolor,pcolor)
	

	
	screen.exitonclick()	

if __name__ == "__main__":
	main()




